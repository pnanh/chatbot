<?php 

	function addStep ($userID , $step){
		GLOBAL $database;
		$database->update("users", ["step" => $step], ["userId" => $userID]);	
	}

	function getStep ($userID) {
		GLOBAL $database;
		$datas = $database->select("users", ["step"], ["userId" => $userID]);
		if(count($datas) > 0){
			return "".$datas[0]["step"];
		}
		return "-1";
	}
	function addUser($userID){
		GLOBAL $database;
		$database->insert("users", [
			"userId" => $userID
		]);
	}

	function updateUser($userID, $fullName, $mobile, $address){
		GLOBAL $database;
		$database->update("users", [
			"fullName" => $fullName,
			"mobile" => $mobile,
			"address" => $address
		],["userId" => $userID]);	
	}

	function updateName($userID, $fullName){
		GLOBAL $database;
		$database->update("users", [
			"fullName" => $fullName
		],["userId" => $userID]);	
	}

	function updateMobile($userID, $mobile){
		GLOBAL $database;
		$database->update("users", [
			"mobile" => $mobile
		],["userId" => $userID]);	
	}

	function updateAddress($userID, $address){
		GLOBAL $database;
		$database->update("users", [
			"address" => $address
		],["userId" => $userID]);	
	}

	function checkUserExisted ($userID) {
		GLOBAL $database;
		$datas = $database->select("users", "*", ["userId" => $userID]);
		if(count($datas) > 0){
			if($datas[0]["fullName"] != "" && $datas[0]["mobile"] != "" && $datas[0]["address"] != ""){
				return 2;
			}
			return 1;
		}
		return 0;
	}


?>