<?php

include_once 'vendor/autoload.php';
include_once 'MamaOrder.php';
include_once 'DAO.php';
include_once 'cache.class.php';
include_once 'BOTHelper.php';

use Mpociot\BotMan\BotManFactory;
use Mpociot\BotMan\BotMan;
use Medoo\Medoo;

$database = new Medoo([
	'database_type' => 'sqlite',
	'database_file' => 'database.db'
]);

$c = new Cache();


$textChat = getChatText();
if(strtolower($textChat) == "y" || strtolower($textChat) == "yes") {
    $textChat = "y";
}else if(strtolower($textChat) == "n" || strtolower($textChat) == "no") {
    $textChat = "n";
}

error_log("Text : ".$textChat) ;

$config = [
    'facebook_token' => 'EAASO8eHmLG8BAHyDHPZB0kKxYUCgYiNGFmS4N3yMGTREeg3YIq9KssnhhQ9ijf32vYNa3FFsa2ajNZCAH5eaRi0Nr7D2sZAjUM1nEIuCANoCH3fZCogok1cQUxIsYXc2a7SmQWoxe8hZCSetHdrhfmg04hXGkJQ7dv0nTuc2mKXljU4Rwi5Da',
    'facebook_app_secret' => '526c821c80414dcdb8962c7220118bc6',
];

// create an instance
$botman = BotManFactory::create($config);

// In your BotMan controller
$botman->verifyServices('mama123');

//Query user information 

//Get Step
/*
0 - start conversation
1 - ask name
2 - ask address
3 - ask phone
4 - list product
5 - list order
6 - buy

*/

// give the bot something to listen for.
// $botman->hears('(mama|MAMA)', function (BotMan $bot) {    
    
//     if(checkUserExisted($userID)){
// 		$bot->reply('Mama xin chao ban !');
//     }else{
    	
//     }

// });

//$userExisted  = 0 - not exited , 1 - existed without user's information , 2 - existed user with full information

$botman->hears('(.*)', function (BotMan $bot) { 

    GLOBAL $textChat;
    GLOBAL $c;

    $user = $bot->getUser();
    //Query user id
    $userID = $user->getId();

    //get temp text from cache
    $tempText = $c->retrieve($userID);
    error_log("tempText: ".$tempText) ;
    //$c->erase($userID);

    $step = getStep($userID);
    error_log("step value: ".$step) ;   

    $userExisted = checkUserExisted($userID);
    error_log("userExisted: ".$userExisted) ;

    error_log("VAO HEARS") ;  
    if(strtolower(substr($textChat,0,3)) == "add"){
        $bot->reply('Da mua san pham so 1');
        $bot->reply('VIEW ORDER');
        addStep($userID , "5");
    }
    elseif (strtolower(substr($textChat,0,3)) == "car") {
        $bot->reply('VIEW ORDER');
        addStep($userID , "5");
    }
    elseif (strtolower(substr($textChat,0,3)) == "buy") {
        $bot->reply('VIEW BUY CONFIRM');
        addStep($userID , "6");
    }
    else{
        switch ($textChat) {
            case "y":{
                error_log("VAO case y with step ".$step) ; 
                switch ($step) {
                    case "1":{
                        updateName($userID, $tempText);
                        addStep($userID , "2");
                        $bot->reply('Xin cho biet dia chi cua ban: ?');
                        break;
                    }
                    case "2":{
                        updateAddress($userID, $tempText );
                        addStep($userID , "3");
                        $bot->reply('Xin cho so dien thoai cua ban: ?');
                        break;
                    }
                    case "3":{
                        updateMobile($userID, $tempText );
                        addStep($userID , "0");
                        $bot->reply('Cam on thong tin cua ban, de bat dau mua san pham ban hay go MAMA nha !');
                        break;
                    }
                    case "6":{
                        addStep($userID , "0");
                        $bot->reply('Cam on ban da dat hang, MAMA se lien he voi ban ngay khi co the!');
                        break;
                    }
                    default:
                    break;
                }
                
                break;
            }
            case "n":{
                switch ($step) {
                    case "1":{
                        $bot->reply('Xin cho biet ten cua ban: ?');
                        break;
                    }
                    case "2":{
                        $bot->reply('Xin cho biet dia chi cua ban: ?');
                        break;
                    }
                    case "3":{
                        $bot->reply('Xin cho so dien thoai cua ban: ?');
                        break;
                    }
                    default:
                    break;
                }
                break;
            }
            case "mama":
            case "MAMA":{
                if($userExisted == 2){
                    addStep($userID , "4");
                    productsPage1($userID);
                }
                break;
            }
            default:
            {
                error_log("VAO default voi step: ".$step) ;  
                switch ($step) {
                    case "-1":{
                            $bot->reply('Mama xin chao ban, ban lam on cho mama it thong tin nhe ! Xin cho biet ten cua ban: ?');
                            addUser($userID);
                            addStep($userID , "1");
                            break;      
                    }   
                    case "0":{
                            if($userExisted == 1){
                                addStep($userID , "1");
                                $bot->reply('Mama xin chao ban, ban lam on cho mama it thong tin nhe ! Xin cho biet ten cua ban: ?');
                                
                            }else{
                                addStep($userID , "4");
                                productsPage1($userID);
                            }
                            break;
                    }
                    case "1":{  
                            $bot->reply('Xin chao '.$textChat. ", dung khong a ? (y/n)");   
                            $c->store($userID, $textChat);
                            break;
                    }
                    case "2":{
                            $bot->reply('Dia chi la: '.$textChat. ", dung khong a ? (y/n)");   
                            $c->store($userID, $textChat);
                            break;

                    }
                    case "3":{
                            $bot->reply('So dien thoai cua ban la: '.$textChat. ", dung khong a ? (y/n)");   
                            $c->store($userID, $textChat);
                            break;

                    }
                    default:
                            sendMainDialog($userID);   
                            break;      
                }
                break;
            }
        }
    }
});




// $botman->fallback(function($bot) {
//     $bot->reply('Để bắt đầu mua hàng, bạn hãy gõ: mama hoặc MAMA nhé !');
// });

// start listening
$botman->listen();


?>