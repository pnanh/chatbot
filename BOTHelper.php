<?php

use Mpociot\BotMan\Facebook\GenericTemplate;
use Mpociot\BotMan\Facebook\ButtonTemplate;
use Mpociot\BotMan\Facebook\ElementButton;

function getChatText(){
		/* receive and send messages */
		$message = "|~~|-1|~~|";
		$input = json_decode(file_get_contents('php://input'), true);
		if (isset($input['entry'][0]['messaging'][0]['sender']['id'])) {
		    $sender = $input['entry'][0]['messaging'][0]['sender']['id']; //sender facebook id
	        if(!empty($input['entry'][0]['messaging'][0]['message'])){
	        	$message = $input['entry'][0]['messaging'][0]['message']['text']; //text that user sent
	        }
	    	else if (!empty($input['entry'][0]['messaging'][0]['postback'])) {
	    		$message = $input['entry'][0]['messaging'][0]['postback']['payload']; //text that user sent
	    	}
		}
		return $message;
}

function sendTemplateMessage($response){
	GLOBAL $config;
	error_log("PAGE TOKEN: ".$config['facebook_token']) ;  
	error_log("DATA: ".json_encode($response)) ; 
	$ch = curl_init('https://graph.facebook.com/v2.6/me/messages?access_token='.$config['facebook_token']);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
	curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
	$result = curl_exec($ch);
	curl_close($ch);
}

function sendMainDialog($senderId){

    $answer = ["attachment"=>[
      "type"=>"template",
      "payload"=>[
        "template_type"=>"generic",
        "elements"=>[
          [
            "title"=>"MAMA CHOCOLATE",
            "item_url"=>"http://mamachoko.com/",
            "image_url"=>"http://mamachoko.com/images/logos/1/header_vdq3-g7.jpg",
            "subtitle"=>"Mama có thể giúp gì bạn ?",
            "buttons"=>[
              [
                "type"=>"postback",
                "title"=>"Mua Hàng",
                "payload"=>"mama"
              ],
              [
                "type"=>"postback",
                "title"=>"Giỏ Hàng",
                "payload"=>"cart"
              ],
              [
                "type"=>"postback",
                "title"=>"Thanh Toán",
                "payload"=>"buy"
              ]              
            ]
          ]
        ]
      ]
    ]];


	$response = [
		'recipient' => [ 'id' => $senderId ],
		'message' =>  $answer
	];

	sendTemplateMessage($response);

}

function productsPage1($senderId){

	$answer = ["attachment"=>[
      "type"=>"template",
      "payload"=>[
        "template_type"=>"list",
        "elements"=>[
          [
             "title"=> "5S MAXI Mix 5 vị",
                    "image_url"=> "http://mamachoko.com/images/thumbnails/80/80/detailed/1/5S.jpg",
                    "subtitle"=> "250,000 VND",
                    "default_action"=> [
                        "type"=> "web_url",
                        "url"=> "http://mamachoko.com/index.php?dispatch=products.view&product_id=7",                       
                        "webview_height_ratio"=> "tall",
                        // "messenger_extensions"=> true,
                        // "fallback_url"=> "https://peterssendreceiveapp.ngrok.io/"
                    ],
            "buttons"=>[
              [
                "type"=>"postback",
                "payload"=>"add|5S MAXI Mix 5 vị - 250k",
                "title"=>"Mua"
              ],
            ]
          ],
            [
            "title"=>"MAXI Trà Xanh ",
            "item_url"=>"http://mamachoko.com/index.php?dispatch=products.view&product_id=2",
            "image_url"=>"http://mamachoko.com/images/thumbnails/80/80/detailed/1/2501.jpg",
            "subtitle"=>"200,000 VND",
            "buttons"=>[
              [
                "type"=>"postback",
                "payload"=>"add|MAXI Trà Xanh - 200k",
                "title"=>"Mua"
              ],
            ]
          ],
            [
            "title"=>"Chocolate Kem Sữa Tươi - FROZEN CHOCOLATE",
            "item_url"=>"http://mamachoko.com/index.php?dispatch=products.view&product_id=81",
            "image_url"=>"http://mamachoko.com/images/thumbnails/80/80/detailed/1/FROZEN_CHOKO.jpg",
            "subtitle"=>"33,250 VND",
            "buttons"=>[
              [
                "type"=>"postback",
                "payload"=>"add|Chocolate Kem Sữa Tươi - FROZEN CHOCOLATE - 34k",
                "title"=>"Mua"
              ],
            ]
          ],
          [
            "title"=>"Chocolate Kem Sữa Tươi - FROZEN CHOCOLATE 2",
            "item_url"=>"http://mamachoko.com/index.php?dispatch=products.view&product_id=81",
            "image_url"=>"http://mamachoko.com/images/thumbnails/80/80/detailed/1/FROZEN_CHOKO.jpg",
            "subtitle"=>"88,250 VND",
            "buttons"=>[
              [
                "type"=>"postback",
                "payload"=>"add|Chocolate Kem Sữa Tươi - FROZEN CHOCOLATE 2 - 88k",
                "title"=>"Mua"
              ],
            ]
          ]
        ],
	    "buttons"=>[
	      	[
		        "type"=>"postback",
		        "payload"=>"page2",
		        "title"=>"Xem thêm"
	        ]
	      ]
      ]
    ]];

    $response = [
		'recipient' => [ 'id' => $senderId ],
		'message' =>  $answer
	];

	sendTemplateMessage($response);

}


?>